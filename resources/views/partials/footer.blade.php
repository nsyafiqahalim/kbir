<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="copyright">
                <p>© <span><script>document.write(new Date().getFullYear())</script></span> Unijaya Industri Sdn Bhd. Theme by
                  <a href="#" onClick="MyWindow=window.open('https://shinnovate.us/',
                  'MyWindow','width=1280,height=850'); return false;" class="transition">
                    {{ trans('global.developer') }}</a>
                </p>
            </div>
        </div>
    </div>
</div>
