# Create a pull request to merge your change
To add reviewers to check and approve your code prior to merging, your next step is to create a pull request. In addition to a place for code review, a pull request shows a comparison of your changes against the original repository (also known as a diff) and provides an easy way to merge code when ready.

## Step 1. Create the pull request
You need a branch to create a pull request. Good thing you created a branch in the previous section of this tutorial.

1. From your repository, click **Create a pull request** under **Pull Request** Menu. Bitbucket displays the request form. Make sure that you are in a repository before you click **Create a pull request**.

2. Complete the form:
  1. You've already got an automatic **Title**, your last commit message.
  2. Add a **Description** if you'd like.
  3.  Click **Create pull request**.

Bitbucket opens the pull request, and if you added a reviewer, they will receive an email notification with details about the pull request for them to review.

_p/s: refer this link https://zapier.com/apps/bitbucket/tutorials/bitbucket-pull-request_

## Step 2. Merge your pull request
Not so fast! You may have noticed the **Merge** button at the top. Before you click it, **you need to wait for an approval of your changes**. In addition to the email notification your teammates receive, they'll also see the pull request appear under **Pull requests to review** on the **Your work** dashboard.

From the pull request, the reviewer can view the diff and add comments to start a discussion before clicking the **Approve** button.

When someone approves your pull request, you'll get an email notification. Once you've got the approvals you need (in this case just one!), you can merge. From the pull request, click **Merge**. And that's it! If you want to see what it looks like when your branch merges with the main branch, click **Commits** to see the commit tree.
