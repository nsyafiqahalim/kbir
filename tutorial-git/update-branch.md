# Update Your Current Branch with Master Branch
The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content.

Setiap kali **access** ke branch yang lain, pastikan branch awak mesti update/sync dengan master branch. Kalau tidak, bila awak buat changes & saya buat changes pada masa yang sama, then awak buat pull request, kemudian saya merge code awak, kebarangkalian ada pertindihan code antara awak dan saya serta menyebabkan kehilangan code yang baharu. Jadi pastikan branch awak mesti **sync** dengan **Master** branch.

1. `git checkout master`
2. `git pull` <- sync dengan cloud repo
3. `git checkout your_branch_name`
4. `git pull origin master` <- update dari cloud repo

Bila dah sync dengan master branch, awak boleh buat perubahan code. Selepas buat perubahan code, awak boleh **commit dan push code** awak ke Bitbucket. Selepas itu baru awak buat **Pull Request**.


## Fix a conflict during pull
Kadang-kala berlakunya conflict bila kita pull dari master branch. Macam saya cakap, pertindihan code di file yang sama. Jadi bila berlakunya conflict, awak boleh menggunakan SourceTree untuk fixingkan.

Dekat **SourceTree** akan highlight file mana yang berlakunya conflict. So boleh resolve dengan **right click** pada file yang conflict, pilih **Resolve Conflict**, pilih **"Resolve Using Theirs"**

Bila dah resolve, awak commit dan push kan semula files. Itu sahaja.
