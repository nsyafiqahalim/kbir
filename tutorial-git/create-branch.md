# CREATE NEW BRANCH
Create a new branch when you're doing development work that is somewhat experimental in nature. Having **dead code** sitting in the master/original branch is not ideal by any means, since it can confuse other developers and may just sit there rotting for the lifetime of the application.

If your team were to experience a bug, you wouldn't want them to waste time exploring the experimental work to determine if the bug existed within that directory. **Creating a new branch** allows you to isolate your changes from the master branch. If your experimentation goes well you always have the option to merge your changes into the master branch. **If things don't go so well you can always discard the branch or keep it within your local repository.**

There are two ways to create a Git branch: In Bitbucket or at your local command line(Git Bash / Editor Terminal).

## To create a branch from Bitbucket

1. From the repository, click **Branch** menu and select **Create a branch**

2. From the popup that appears, select a **Type** (if using the Branching model), enter a **Branch name** and click **Create**.

3. After you create a branch, you need to check it out from your local system. Use the fetch and checkout commands that Bitbucket provides, similar to the following:

   `git fetch && git checkout your_new_branch_names`

4. Make your changes locally and then add, commit, and push your changes to the <feature> branch:

  `git add .` > `git commit -m "your any commit changes"` > `git push origin your_new_branch_names`

5. Click the **Source** page of your repository. You should see both the master and the <your_new_branch_names> branch in the branches dropdown. When you make commits to the feature branch, you'll see the files specific to that branch.

### To create a branch locally
You can create a branch locally as long as you have a cloned version of the repo.

1. From your terminal window, list the branches on your repository.
  `git branch`

  -> This output indicates there is a single branch, the master and the asterisk indicates it is currently active.

2. Create a new feature branch in the repository: `git branch your_new_branch_names`

3. Switch to the feature branch to work on it: `git checkout your_new_branch_names`

  -> You can list the branches again with the git branch command.

4. Commit the change to the feature branch: `git add . ` > `git commit -m "adding a change from the feature branch"`

5. Switch back to the master branch: `git checkout master`

6. Push the feature branch to Bitbucket: `git push origin your_new_branch_names`

7. View the **Source** page of your repository in Bitbucket. You should see both the `master` and the feature branch. When you select the feature branch, you see the **Source** page from that perspective. Select the feature branch to view its **Recent commits**.
