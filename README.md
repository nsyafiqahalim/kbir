# Knowledge-Based Retrieval Information System (KBRI)

A special system developed for a company called Unijaya Industry Sdn Bhd (UISB).

**Features:**

  1. Admin:
  
      - Login to the system.
      - Manage the system.
      - Register, update, upload, view, and delete the files.
      - Upload the scanned document/drawing/files into the system (document must in PDF format).
      - Create and edit project information to the uploaded files/documents.
      - View all document/drawing/files in the system.
      - Create user and manage user information.
      
  2. User:
      - Login to the system.
      - Search the document by project title/location.
      - View and scroll the document in the system.
      - Filtration by cost.
      - Project owner can view project file access information (e.g. who has searched and viewed his/her file)

## Default access credentials

- __Email__: admin@admin.com | user@user.com
- __Pass__: password

## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database/Stripe credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)
- Run __php artisan storage:link__
- That's it: launch the main URL and login with default credentials __admin@admin.com__ - __password__

---
### License

Basically, feel free to use and re-use any way you want.
